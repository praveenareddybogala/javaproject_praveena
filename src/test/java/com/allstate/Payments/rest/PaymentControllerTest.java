package com.allstate.Payments.rest;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
public class PaymentControllerTest {
    @Mock
    private RestTemplate restTemplate;
    int port=8088;
    @Test
    public void getStatusShouldReturnDefaultMessage() throws Exception{
        Mockito.when(restTemplate.getForObject("http://localhost:"+port+"/api/payment/status",String.class))
                .thenReturn("Rest Api is running");
    }
}
