package com.allstate.Payments.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PaymentEntityTest {
    private Payment payment;
    @BeforeEach
    void setUp(){
        payment= new Payment(10, new Date(),"card",200d,21,"mouni");
    }
    @Test
    void getId(){
        assertEquals(10,payment.getId());
    }
    @Test
    void getCustName(){
        assertEquals(1,payment.getId());
    }

    @Test
    void getPaymentDate(){
        assertEquals(new Date(),payment.getPaymentDate());
    }
    @Test
    void getType(){
        assertEquals("card",payment.getType());
    }
    @Test
    void getAmount(){
        assertEquals(200d,payment.getAmount());
    }
    @Test
    void getCustId(){
        assertEquals(21,payment.getCustId());
    }


}
