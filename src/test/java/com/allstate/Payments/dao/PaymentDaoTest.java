package com.allstate.Payments.dao;

import com.allstate.Payments.entities.Payment;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.Date;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PaymentDaoTest {
    @Autowired
    private PaymentDao dao;
    @Autowired
    MongoTemplate tpl;
    @Test
    public void save_findById_findByType_size(){
        Payment payment =new Payment(4,new Date(),"card",300d,31,"arjun");
        dao.save(payment);
        assertEquals(payment.getId(),dao.findById(4).getId());
       // assertEquals(1,dao.findByType("card").size());
       // assertEquals(1,dao.rowCount());
    }
//    @AfterAll
//    public void Cleanup(){
//        Payment payment=dao.findById(2);
//        tpl.remove(payment);
//    }
    @AfterEach
    public void cleanUp(){
        for(String collectionName:tpl.getCollectionNames()){
            if(!collectionName.startsWith("system.")){
                tpl.dropCollection(collectionName);
            }
        }
    }
}
