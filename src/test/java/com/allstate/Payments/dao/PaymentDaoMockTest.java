package com.allstate.Payments.dao;

import com.allstate.Payments.entities.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;

public class PaymentDaoMockTest {
    @Mock private PaymentDao dao;
    @BeforeEach
    void setUp(){
        initMocks(this);
    }
    @Test
    void rowCount_Success(){
        Payment payment=new Payment(5,new Date(),"cash",1000d,82,"Praveena");
        doReturn(1).when(dao).rowCount();
        assertEquals(1,dao.rowCount());
    }
    @Test
    public void save_Success(){
        Payment payment=new Payment(6,new Date(),"cash",800d,76,"veena");
        doReturn(0).when(dao).save(any(Payment.class));
        assertEquals(0,dao.save(payment));
    }
    @Test
    public void findByID_Success(){
        Payment payment =new Payment(7,new Date(),"card",789d,81,"reddy");
        doReturn(payment).when(dao).findById(7);
        assertEquals(payment,dao.findById(7));
    }
    @Test
    public void findByType_Success(){
        List<Payment> payments =new ArrayList<Payment>(Arrays.asList(
                new Payment(8,new Date(),"cash",123d,87,"teja"),
                new Payment(9,new Date(),"cash",789d,43,"naveen")

        ));
        doReturn(payments).when(dao).findByType("cash");
        assertEquals(payments,dao.findByType("cash"));
    }
}
