package com.allstate.Payments.services;

import com.allstate.Payments.entities.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
@SpringBootTest
public class PaymentServiceTest {
    @Autowired PaymentService service;
    @Autowired
    MongoTemplate tpl;
    @BeforeEach
    public void setup(){
        Date date=new Date();
        Payment payment1= new Payment(1,date,"cash",400d,41,"tharun");
        Payment payment2= new Payment(2,date,"cash",500d,51,"thanuja");
        Payment payment3= new Payment(3,date,"cash",600d,61,"thanu reddy");
        service.save(payment1);
        service.save(payment2);
        service.save(payment3);
    }
    @Test
    public void getCount(){
        assertEquals(3,service.rowCount());
    }
    @Test
    public void getById(){
        Payment  paymentInfo = service.findById(2);
        assertEquals(2,paymentInfo.getId());
    }
    @Test
    public void getByType(){
        List<Payment>  paymentInfo = service.findByType("cash");
        assertEquals("cash",paymentInfo.get(0).getType());
    }
    @Test
    public void getByDate(){
        SimpleDateFormat formatter=new SimpleDateFormat("dd/mm/yyy HH:mm:ss");
        Date date=new Date();
        Payment  paymentInfo = service.findById(2);
        String expectedFormat = formatter.format(date);
        String actualFormat = formatter.format(paymentInfo.getPaymentDate());
        assertEquals(expectedFormat,actualFormat);
    }

}
