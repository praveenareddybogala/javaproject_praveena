package com.allstate.Payments.dao;

import com.allstate.Payments.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class PaymentDaoImpl implements PaymentDao{
    @Autowired
    private MongoTemplate tpl;

    public PaymentDaoImpl() {
    }
    public PaymentDaoImpl(MongoTemplate tpl) {
        this.tpl = tpl;
    }

    @Override
    public int rowCount() {
        List<Payment> payments=tpl.findAll(Payment.class);
        return payments.size();
    }
    @Override
    public List<Payment> findall() {
        return tpl.findAll(Payment.class);
    }

    @Override
    public Payment findById(int id) {
        Query query =new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Payment payment= tpl.findOne(query,Payment.class);
        return payment;
    }

    @Override
    public List<Payment> findByType(String type) {
        Query query =new Query();
        query.addCriteria(Criteria.where("type").is(type));
        List<Payment> payment= tpl.find(query,Payment.class);
        return payment;
    }

    @Override
    public int save(Payment payment) {
        tpl.save(payment);
        return 0;
    }
}
