package com.allstate.Payments.dao;

import com.allstate.Payments.entities.Payment;

import java.util.List;

public interface PaymentDao {
    int rowCount();
    List<Payment> findall();
    Payment findById(int id);
 List<Payment> findByType(String type);
 int save(Payment payment);

}
