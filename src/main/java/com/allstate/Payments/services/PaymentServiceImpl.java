package com.allstate.Payments.services;

import com.allstate.Payments.dao.PaymentDao;
import com.allstate.Payments.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PaymentServiceImpl implements PaymentService {
   @Autowired PaymentDao dao;

    @Override
    public int rowCount() {
        return dao.rowCount();
    }
    @Override
    public List<Payment> all() {
        return dao.findall();
    }
    @Override
    public Payment findById(int id) {
        return id>0?dao.findById(id):null;
    }

    @Override
    public List<Payment> findByType(String type) {
        return type!=null & !type.trim().isEmpty()?dao.findByType(type):null;
    }

    @Override
    public int save(Payment payment) {
        return dao.save(payment);
    }
}
