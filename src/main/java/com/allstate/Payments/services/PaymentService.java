package com.allstate.Payments.services;

import com.allstate.Payments.entities.Payment;

import java.util.List;

public interface PaymentService {
    int rowCount();
    List <Payment> all();
    Payment findById(int id);
    List<Payment> findByType(String type);
    int save(Payment payment);
}
