package com.allstate.Payments.entities;

import org.springframework.data.annotation.Id;

import java.util.Date;

public class Payment {
    @Id
    private int id;
    private Date paymentDate;
    private String type;
    private double amount;
    private int custId;
    private String custName;

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public Payment() {
    }

    public Payment(int id, Date paymentDate, String type, double amount, int custId,String custName) {
        this.id = id;
        this.paymentDate = paymentDate;
        this.type = type;
        this.amount = amount;
        this.custId = custId;
        this.custName=custName;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }
    @Override
    public String toString(){
        return "Payments{"+
                "id="+ id+
                ", paymentDate="+paymentDate+
                ", type='"+type+'\'' +
                ", amount="+amount+
                ", custId="+custId+
                ", custName=" +custName+
                '}';
    }

}
