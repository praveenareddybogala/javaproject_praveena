package com.allstate.Payments.rest;

import com.allstate.Payments.entities.Payment;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IPaymentController {
    String status();
    int rowCount();
    List<Payment> all();
    ResponseEntity<Payment> findById(int id);
    ResponseEntity<List> findByType(String type);
    int save(Payment payment);
}
