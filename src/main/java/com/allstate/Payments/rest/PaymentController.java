package com.allstate.Payments.rest;

import com.allstate.Payments.entities.Payment;
import com.allstate.Payments.services.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@RestController
@CrossOrigin(origins="http://localhost:3000")
@RequestMapping("/api/payment")
public class PaymentController implements IPaymentController{
    @Autowired
    PaymentService service;
    Logger logger= Logger.getLogger(PaymentController.class.getName());
    @RequestMapping(value="/status",method= RequestMethod.GET)
    @Override
    public String status() {
        logger.info("Payment Status method called");
        return "Rest api is running";
    }
    @RequestMapping(value="/count",method=RequestMethod.GET)
    @Override
    public int rowCount() {
        logger.info("Payment rowcount method called");
        return service.rowCount();
    }
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @Override
    public List<Payment> all() {
        return service.all();
    }
    @RequestMapping(value="/findById/{id}",method=RequestMethod.GET)
    @Override
    public ResponseEntity<Payment> findById(@PathVariable("id") int id) {
        logger.info("Payment findById method called");
        Payment payment=service.findById(id);
        if(payment==null){
            return new ResponseEntity<Payment>(HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity<Payment>(payment, HttpStatus.OK);

        }
    }
    @RequestMapping(value="/findByType/{type}",method=RequestMethod.GET)
    @Override
    public ResponseEntity<List> findByType(@PathVariable("type") String type) {
        logger.info("Payment findByType method called");
        List<Payment> payment=service.findByType(type);
        if(payment==null){
            return new ResponseEntity<List>(HttpStatus.NOT_FOUND);
        }
        else{
            return new ResponseEntity<List>(payment,HttpStatus.OK);
        }
    }
    @RequestMapping(value="/save",method=RequestMethod.POST)
    @Override
    public int save(@RequestBody Payment payment) {
        logger.info("Payment Save method called");
        return service.save(payment);
    }
}
